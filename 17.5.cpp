﻿#include <iostream>

class Example {
private:

    int a;

public:

    int GetA() {
        return a;
    }

    void SetA(int newA) {
        a = newA;
    }
};

class Vector {
public:

    Vector(int _x, int _y, int _z) : x(_x), y(_y), z(_z) {
    }

    int GetM() {
        int m = x * x + y * y + z * z;
        return m;
    }

private:

    int x;
    int y;
    int z;

};

int main() {

    setlocale(0, "");
    Example temp;
    temp.SetA(6);
    std::cout << temp.GetA() << "\n" << "Теперь вторая часть:" << "\n";

    Vector v(2, 6, 7);
    std::cout << v.GetM();
}
